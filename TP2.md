# Partie 1 : Mise en place de la solution

## 1. Install MariaDB

🌞 **Installer MariaDB sur la machine `db.tp2.cesi`**

```
sudo dnf install mariadb-server
[sudo] password for vincent:
Last metadata expiration check: 1 day, 2:55:21 ago on Mon 06 Dec 2021 11:54:20 AM CET.
Dependencies resolved.
Installed:
  mariadb-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-backup-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-common-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-connector-c-3.1.11-2.el8_3.x86_64
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch
  mariadb-errmsg-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-gssapi-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-server-utils-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  perl-DBD-MySQL-4.046-3.module+el8.4.0+577+b8fe2d92.x86_64
  perl-DBI-1.641-3.module+el8.4.0+509+59a8d9b3.x86_64
  perl-Math-BigInt-1:1.9998.11-7.el8.noarch
  perl-Math-Complex-1.59-420.el8.noarch
  psmisc-23.1-5.el8.x86_64

Complete!
```

🌞 **Le service MariaDB**

```
sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service
```
```
sudo systemctl start mariadb
[vincent@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 14:53:27 CET; 2s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 2993 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 2858 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
  Process: 2833 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 2962 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 11221)
   Memory: 83.5M
   CGroup: /system.slice/mariadb.service
           └─2962 /usr/libexec/mysqld --basedir=/usr
```
```
 ss -lutpn
Netid      State       Recv-Q      Send-Q           Local Address:Port           Peer Address:Port      Process
udp        UNCONN      0           0                    127.0.0.1:323                 0.0.0.0:*
udp        UNCONN      0           0                        [::1]:323                    [::]:*
tcp        LISTEN      0           128                    0.0.0.0:1230                0.0.0.0:*
tcp        LISTEN      0           80                           *:3306                      *:*
tcp        LISTEN      0           128                       [::]:1230                   [::]:*
```
```
ps -aux | grep "/usr/libexec/mysqld --basedir=/usr"
mysql       2962  0.0  5.0 1758480 93320 ?       Ssl  14:53   0:00 /usr/libexec/mysqld --basedir=/usr
vincent     3054  0.0  0.0 221928  1148 pts/0    S+   14:58   0:00 grep --color=auto /usr/libexec/mysqld --basedir=/usr

```

🌞 **Firewall**

```
sudo firewall-cmd --add-port=3306/tcp --permanent
success

sudo firewall-cmd --reload
success

```


## 2. Conf MariaDB



🌞 **Configuration élémentaire de la base**


# Partie 1 : Mise en place de la solution

## 1. Install MariaDB

🌞 **Installer MariaDB sur la machine `db.tp2.cesi`**

```
sudo dnf install mariadb-server
[sudo] password for vincent:
Last metadata expiration check: 1 day, 2:55:21 ago on Mon 06 Dec 2021 11:54:20 AM CET.
Dependencies resolved.
Installed:
  mariadb-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-backup-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-common-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-connector-c-3.1.11-2.el8_3.x86_64
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch
  mariadb-errmsg-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-gssapi-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-server-utils-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  perl-DBD-MySQL-4.046-3.module+el8.4.0+577+b8fe2d92.x86_64
  perl-DBI-1.641-3.module+el8.4.0+509+59a8d9b3.x86_64
  perl-Math-BigInt-1:1.9998.11-7.el8.noarch
  perl-Math-Complex-1.59-420.el8.noarch
  psmisc-23.1-5.el8.x86_64

Complete!
```

🌞 **Le service MariaDB**

```
sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service
```
```
sudo systemctl start mariadb
[vincent@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 14:53:27 CET; 2s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 2993 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 2858 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
  Process: 2833 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 2962 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 11221)
   Memory: 83.5M
   CGroup: /system.slice/mariadb.service
           └─2962 /usr/libexec/mysqld --basedir=/usr
```
```
 ss -lutpn
Netid      State       Recv-Q      Send-Q           Local Address:Port           Peer Address:Port      Process
udp        UNCONN      0           0                    127.0.0.1:323                 0.0.0.0:*
udp        UNCONN      0           0                        [::1]:323                    [::]:*
tcp        LISTEN      0           128                    0.0.0.0:1230                0.0.0.0:*
tcp        LISTEN      0           80                           *:3306                      *:*
tcp        LISTEN      0           128                       [::]:1230                   [::]:*

```
```
ps -aux | grep "/usr/libexec/mysqld --basedir=/usr"
mysql       2962  0.0  5.0 1758480 93320 ?       Ssl  14:53   0:00 /usr/libexec/mysqld --basedir=/usr
vincent     3054  0.0  0.0 221928  1148 pts/0    S+   14:58   0:00 grep --color=auto /usr/libexec/mysqld --basedir=/usr

```

🌞 **Firewall**

```
sudo firewall-cmd --add-port=3306/tcp --permanent
success

sudo firewall-cmd --reload
success

```


## 2. Conf MariaDB



🌞 **Configuration élémentaire de la base**
```
mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] n
 ... skipping.

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] n
 ... skipping.

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] ^C
Aborting!

Cleaning up...
[vincent@db ~]$ mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: NO)
Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

You already have a root password set, so you can safely answer 'n'.

Change the root password? [Y/n] n
 ... skipping.

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure

```


---

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**

```
sudo mysql -u root -p
[sudo] password for vincent:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 25
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

```
```
CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'aze+123';
Query OK, 0 rows affected (0.000 sec)

```

```
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

```
```
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
Query OK, 0 rows affected (0.001 sec)

```
```
FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

```



🌞 **Installez sur la machine `web.tp2.cesi` la commande `mysql`**

```
sudo mysql -u nextcloud -p
[sudo] password for vincent:
sudo: mysql: command not found
[vincent@web ~]$ dnf provides mysql
Rocky Linux 8 - AppStream                                                               257 kB/s | 8.3 MB     00:32
Rocky Linux 8 - BaseOS                                                                  238 kB/s | 3.5 MB     00:15
Rocky Linux 8 - Extras                                                                  7.9 kB/s |  10 kB     00:01
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

```
```
udo dnf install mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
[sudo] password for vincent:
Last metadata expiration check: 0:16:17 ago on Tue 07 Dec 2021 03:13:35 PM CET.
Dependencies resolved.
=====================================================================================================================
 Package                           Architecture  Version                                      Repository        Size
=====================================================================================================================
Installing:
 mysql                             x86_64        8.0.26-1.module+el8.4.0+652+6de068a7         appstream         12 M
Installing dependencies:
 mariadb-connector-c-config        noarch        3.1.11-2.el8_3                               appstream         14 k
 mysql-common                      x86_64        8.0.26-1.module+el8.4.0+652+6de068a7         appstream        133 k
Enabling module streams:
 mysql                                           8.0

Transaction Summary
=====================================================================================================================
Install  3 Packages

Total download size: 12 M
Installed size: 63 M
Is this ok [y/N]: y
Downloading Packages:
(1/3): mariadb-connector-c-config-3.1.11-2.el8_3.noarch.rpm                           94 kB/s |  14 kB     00:00
(2/3): mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64.rpm                  401 kB/s | 133 kB     00:00
(3/3): mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64.rpm                         3.3 MB/s |  12 MB     00:03
---------------------------------------------------------------------------------------------------------------------
Total                                                                                3.0 MB/s |  12 MB     00:04
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                             1/1
  Installing       : mariadb-connector-c-config-3.1.11-2.el8_3.noarch                                            1/3
  Installing       : mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                    2/3
  Installing       : mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                           3/3
  Running scriptlet: mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                           3/3
  Verifying        : mariadb-connector-c-config-3.1.11-2.el8_3.noarch                                            1/3
  Verifying        : mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                           2/3
  Verifying        : mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                    3/3

Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch             mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!

```


🌞 **Tester la connexion**

```
mysql -P 3306 -u nextcloud -h 10.2.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 26
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

```
```
mysql> use nextcloud;
Database changed

mysql> show tables;
Empty set (0.00 sec)
```



# II. Setup Apache



## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp2.cesi`**

```
sudo dnf install httpd
[sudo] password for vincent:
Last metadata expiration check: 0:26:30 ago on Tue 07 Dec 2021 03:13:35 PM CET.
Dependencies resolved.
=====================================================================================================================
 Package                    Architecture    Version                                         Repository          Size
=====================================================================================================================
Installing:
 httpd                      x86_64          2.4.37-43.module+el8.5.0+714+5ec56ee8           appstream          1.4 M
Installing dependencies:
 apr                        x86_64          1.6.3-12.el8                                    appstream          128 k
 apr-util                   x86_64          1.6.1-6.el8.1                                   appstream          104 k
 httpd-filesystem           noarch          2.4.37-43.module+el8.5.0+714+5ec56ee8           appstream           38 k
 httpd-tools                x86_64          2.4.37-43.module+el8.5.0+714+5ec56ee8           appstream          106 k
 mod_http2                  x86_64          1.15.7-3.module+el8.5.0+695+1fa8055e            appstream          153 k
 rocky-logos-httpd          noarch          85.0-3.el8                                      baseos              22 k
Installing weak dependencies:
 apr-util-bdb               x86_64          1.6.1-6.el8.1                                   appstream           23 k
 apr-util-openssl           x86_64          1.6.1-6.el8.1                                   appstream           26 k
Enabling module streams:
 httpd                                      2.4

Transaction Summary
=====================================================================================================================
Install  9 Packages

Total download size: 2.0 M
Installed size: 5.4 M
Is this ok [y/N]: y
Downloading Packages:
(1/9): apr-util-bdb-1.6.1-6.el8.1.x86_64.rpm                                         137 kB/s |  23 kB     00:00
(2/9): apr-util-openssl-1.6.1-6.el8.1.x86_64.rpm                                     160 kB/s |  26 kB     00:00
(3/9): apr-1.6.3-12.el8.x86_64.rpm                                                   348 kB/s | 128 kB     00:00
(4/9): apr-util-1.6.1-6.el8.1.x86_64.rpm                                             280 kB/s | 104 kB     00:00
(5/9): httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch.rpm             600 kB/s |  38 kB     00:00
(6/9): httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64.rpm                  1.0 MB/s | 106 kB     00:00
(7/9): mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64.rpm                     947 kB/s | 153 kB     00:00
(8/9): rocky-logos-httpd-85.0-3.el8.noarch.rpm                                       169 kB/s |  22 kB     00:00
(9/9): httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64.rpm                        1.4 MB/s | 1.4 MB     00:01
---------------------------------------------------------------------------------------------------------------------
Total                                                                                969 kB/s | 2.0 MB     00:02
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                             1/1
  Installing       : apr-1.6.3-12.el8.x86_64                                                                     1/9
  Running scriptlet: apr-1.6.3-12.el8.x86_64                                                                     1/9
  Installing       : apr-util-bdb-1.6.1-6.el8.1.x86_64                                                           2/9
  Installing       : apr-util-openssl-1.6.1-6.el8.1.x86_64                                                       3/9
  Installing       : apr-util-1.6.1-6.el8.1.x86_64                                                               4/9
  Running scriptlet: apr-util-1.6.1-6.el8.1.x86_64                                                               4/9
  Installing       : httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                    5/9
  Installing       : rocky-logos-httpd-85.0-3.el8.noarch                                                         6/9
  Running scriptlet: httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                               7/9
  Installing       : httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                               7/9
  Installing       : mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64                                       8/9
  Installing       : httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                          9/9
  Running scriptlet: httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                          9/9
  Verifying        : apr-1.6.3-12.el8.x86_64                                                                     1/9
  Verifying        : apr-util-1.6.1-6.el8.1.x86_64                                                               2/9
  Verifying        : apr-util-bdb-1.6.1-6.el8.1.x86_64                                                           3/9
  Verifying        : apr-util-openssl-1.6.1-6.el8.1.x86_64                                                       4/9
  Verifying        : httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                          5/9
  Verifying        : httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                               6/9
  Verifying        : httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                    7/9
  Verifying        : mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64                                       8/9
  Verifying        : rocky-logos-httpd-85.0-3.el8.noarch                                                         9/9

Installed:
  apr-1.6.3-12.el8.x86_64
  apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64
  apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
```


🌞 **Analyse du service Apache**

```
sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```
```
sudo systemctl start httpd

systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 15:41:40 CET; 3s ago
     Docs: man:httpd.service(8)
 Main PID: 3090 (httpd)
   Status: "Started, listening on: port 80"
    Tasks: 213 (limit: 11221)
   Memory: 29.3M
   CGroup: /system.slice/httpd.service
           ├─3090 /usr/sbin/httpd -DFOREGROUND
           ├─3091 /usr/sbin/httpd -DFOREGROUND
           ├─3092 /usr/sbin/httpd -DFOREGROUND
           ├─3093 /usr/sbin/httpd -DFOREGROUND
           └─3094 /usr/sbin/httpd -DFOREGROUND

```
```
ss -lutpn
7Netid      State       Recv-Q      Send-Q           Local Address:Port           Peer Address:Port      Process     
udp        UNCONN      0           0                    127.0.0.1:323                 0.0.0.0:*
udp        UNCONN      0           0                        [::1]:323                    [::]:*
tcp        LISTEN      0           128                    0.0.0.0:1230                0.0.0.0:*
tcp        LISTEN      0           128                       [::]:1230                   [::]:*
tcp        LISTEN      0           128                          *:80                        *:*

```
```
ps -aux | grep "/system.slice/httpd.service"
vincent     3316  0.0  0.0 221928  1092 pts/0    S+   15:43   0:00 grep --color=auto /system.slice/httpd.service
```

---

🌞 **Un premier test**

```
sudo firewall-cmd --add-port=80/tcp --permanent
Warning: ALREADY_ENABLED: 80:tcp
```

```
sudo firewall-cmd --reload
success
```
```
curl 10.2.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%) ;
  background: linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%);
  background-repeat: no-repeat;
  background-attachment: fixed;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3c6eb4",endColorstr="#3c95b4",GradientType=1);
        color: white;
        font-size: 0.9em;
        font-weight: 400;
        font-family: 'Montserrat', sans-serif;
        margin: 0;
        padding: 10em 6em 10em 6em;
        box-sizing: border-box;

      }
```



### B. PHP

NextCloud a besoin d'une version bien spécifique de PHP.  
Suivez **scrupuleusement** les instructions qui suivent pour l'installer.

🌞 **Installer PHP**

```
sudo dnf install epel-release
Last metadata expiration check: 0:39:34 ago on Tue 07 Dec 2021 03:13:35 PM CET.
Dependencies resolved.
=====================================================================================================================
 Package                        Architecture             Version                      Repository                Size
=====================================================================================================================
Installing:
 epel-release                   noarch                   8-13.el8                     extras                    23 k

Transaction Summary
=====================================================================================================================
Install  1 Package

Total download size: 23 k
Installed size: 35 k
Is this ok [y/N]: y
Downloading Packages:
epel-release-8-13.el8.noarch.rpm                                                      68 kB/s |  23 kB     00:00
---------------------------------------------------------------------------------------------------------------------
Total                                                                                 32 kB/s |  23 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                             1/1
  Installing       : epel-release-8-13.el8.noarch                                                                1/1
  Running scriptlet: epel-release-8-13.el8.noarch                                                                1/1
  Verifying        : epel-release-8-13.el8.noarch                                                                1/1

Installed:
  epel-release-8-13.el8.noarch

Complete!
```


```
sudo dnf update
Extra Packages for Enterprise Linux 8 - x86_64                                       3.2 MB/s |  11 MB     00:03
Extra Packages for Enterprise Linux Modular 8 - x86_64                               650 kB/s | 980 kB     00:01
Dependencies resolved.
Upgraded:
  binutils-2.30-108.el8_5.1.x86_64                        bpftool-4.18.0-348.2.1.el8_5.x86_64
  kernel-tools-4.18.0-348.2.1.el8_5.x86_64                kernel-tools-libs-4.18.0-348.2.1.el8_5.x86_64
  libgcc-8.5.0-4.el8_5.x86_64                             libgomp-8.5.0-4.el8_5.x86_64
  libipa_hbac-2.5.2-2.el8_5.1.x86_64                      libsss_autofs-2.5.2-2.el8_5.1.x86_64
  libsss_certmap-2.5.2-2.el8_5.1.x86_64                   libsss_idmap-2.5.2-2.el8_5.1.x86_64
  libsss_nss_idmap-2.5.2-2.el8_5.1.x86_64                 libsss_sudo-2.5.2-2.el8_5.1.x86_64
  libstdc++-8.5.0-4.el8_5.x86_64                          python3-perf-4.18.0-348.2.1.el8_5.x86_64
  python3-sssdconfig-2.5.2-2.el8_5.1.noarch               sssd-2.5.2-2.el8_5.1.x86_64
  sssd-ad-2.5.2-2.el8_5.1.x86_64                          sssd-client-2.5.2-2.el8_5.1.x86_64
  sssd-common-2.5.2-2.el8_5.1.x86_64                      sssd-common-pac-2.5.2-2.el8_5.1.x86_64
  sssd-ipa-2.5.2-2.el8_5.1.x86_64                         sssd-kcm-2.5.2-2.el8_5.1.x86_64
  sssd-krb5-2.5.2-2.el8_5.1.x86_64                        sssd-krb5-common-2.5.2-2.el8_5.1.x86_64
  sssd-ldap-2.5.2-2.el8_5.1.x86_64                        sssd-nfs-idmap-2.5.2-2.el8_5.1.x86_64
  sssd-proxy-2.5.2-2.el8_5.1.x86_64                       unzip-6.0-45.el8_4.x86_64
Installed:
  bind-libs-32:9.11.26-6.el8.x86_64                       bind-libs-lite-32:9.11.26-6.el8.x86_64
  bind-license-32:9.11.26-6.el8.noarch                    bind-utils-32:9.11.26-6.el8.x86_64
  fstrm-0.6.1-2.el8.x86_64                                geolite2-city-20180605-1.el8.noarch
  geolite2-country-20180605-1.el8.noarch                  kernel-4.18.0-348.2.1.el8_5.x86_64
  kernel-core-4.18.0-348.2.1.el8_5.x86_64                 kernel-modules-4.18.0-348.2.1.el8_5.x86_64
  libmaxminddb-1.2.0-10.el8.x86_64                        protobuf-c-1.3.0-6.el8.x86_64
  python3-bind-32:9.11.26-6.el8.noarch                    python3-ply-3.9-9.el8.noarch

Complete!
```

```
sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:03:24 ago on Tue 07 Dec 2021 03:53:55 PM CET.
remi-release-8.rpm                                                                   135 kB/s |  26 kB     00:00
Dependencies resolved.
=====================================================================================================================
 Package                     Architecture          Version                         Repository                   Size
=====================================================================================================================
Installing:
 remi-release                noarch                8.5-2.el8.remi                  @commandline                 26 k

Transaction Summary
=====================================================================================================================
Install  1 Package

Total size: 26 k
Installed size: 21 k
Is this ok [y/N]: y
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                             1/1
  Installing       : remi-release-8.5-2.el8.remi.noarch                                                          1/1
  Verifying        : remi-release-8.5-2.el8.remi.noarch                                                          1/1

Installed:
  remi-release-8.5-2.el8.remi.noarch

Complete!
```

```
 dnf module enable php:remi-7.4
Error: This command has to be run with superuser privileges (under the root user on most systems).
[vincent@web ~]$ sudo dnf module enable php:remi-7.4
Remi's Modular repository for Enterprise Linux 8 - x86_64                            1.5 kB/s | 858  B     00:00
Remi's Modular repository for Enterprise Linux 8 - x86_64                            3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Remi's Modular repository for Enterprise Linux 8 - x86_64                            1.3 MB/s | 946 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                           1.9 kB/s | 858  B     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                           3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                           2.1 MB/s | 2.0 MB     00:00
Last metadata expiration check: 0:00:01 ago on Tue 07 Dec 2021 03:58:13 PM CET.
Dependencies resolved.
=====================================================================================================================
 Package                    Architecture              Version                       Repository                  Size
=====================================================================================================================
Enabling module streams:
 php                                                  remi-7.4

Transaction Summary
=====================================================================================================================

Is this ok [y/N]: y
Complete!
```

```
sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:00:38 ago on Tue 07 Dec 2021 03:58:13 PM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
Installed:
  checkpolicy-2.9-1.el8.x86_64                          environment-modules-4.5.2-1.el8.x86_64
  libicu69-69.1-1.el8.remi.x86_64                       libsodium-1.0.18-2.el8.x86_64
  oniguruma5php-6.9.7.1-1.el8.remi.x86_64               php74-libzip-1.8.0-1.el8.remi.x86_64
  php74-php-7.4.26-1.el8.remi.x86_64                    php74-php-bcmath-7.4.26-1.el8.remi.x86_64
  php74-php-cli-7.4.26-1.el8.remi.x86_64                php74-php-common-7.4.26-1.el8.remi.x86_64
  php74-php-fpm-7.4.26-1.el8.remi.x86_64                php74-php-gd-7.4.26-1.el8.remi.x86_64
  php74-php-gmp-7.4.26-1.el8.remi.x86_64                php74-php-intl-7.4.26-1.el8.remi.x86_64
  php74-php-json-7.4.26-1.el8.remi.x86_64               php74-php-mbstring-7.4.26-1.el8.remi.x86_64
  php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64            php74-php-opcache-7.4.26-1.el8.remi.x86_64
  php74-php-pdo-7.4.26-1.el8.remi.x86_64                php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64
  php74-php-process-7.4.26-1.el8.remi.x86_64            php74-php-sodium-7.4.26-1.el8.remi.x86_64
  php74-php-xml-7.4.26-1.el8.remi.x86_64                php74-runtime-1.0-3.el8.remi.x86_64
  policycoreutils-python-utils-2.9-16.el8.noarch        python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64
  python3-libsemanage-2.9-6.el8.x86_64                  python3-policycoreutils-2.9-16.el8.noarch
  python3-setools-4.3.0-2.el8.x86_64                    scl-utils-1:2.0.2-14.el8.x86_64
  tcl-1:8.6.8-2.el8.x86_64

Complete!
```

## 2. Conf Apache

---

🌞 **Analyser la conf Apache**

IncludeOptional conf.d/*.conf

🌞 **Créer un VirtualHost qui accueillera NextCloud**

```
cd /etc/httpd/conf.d
```

```
sudo touch nextcloud.conf
```
```
sudo nano nextcloud.conf
```
```
sudo systemctl restart httpd
```



🌞 **Configurer la racine web**

```
cd /var/www/
```

```
sudo mkdir nextcloud
```

```
cd nextcloud
```

```
sudo mkdir html
```

```
ps -aux | grep httpd
root        6045  0.0  0.6 283000 11560 ?        Ss   08:49   0:00 /usr/sbin/httpd -DFOREGROUND
apache      6046  0.0  0.4 296884  8552 ?        S    08:49   0:00 /usr/sbin/httpd -DFOREGROUND
apache      6047  0.0  0.7 1944544 14240 ?       Sl   08:49   0:00 /usr/sbin/httpd -DFOREGROUND
apache      6048  0.0  0.8 1813416 16276 ?       Sl   08:49   0:00 /usr/sbin/httpd -DFOREGROUND
apache      6049  0.0  0.6 1813416 12196 ?       Sl   08:49   0:00 /usr/sbin/httpd -DFOREGROUND
```


```
cd /var/www/
```

```
sudo chown apache nextcloud
[vincent@web www]$ ls -l
total 0
drwxr-xr-x. 2 root   root  6 Nov 15 04:13 cgi-bin
drwxr-xr-x. 2 root   root  6 Nov 15 04:13 html
drwxr-xr-x. 3 apache root 18 Dec  8 08:54 nextcloud
```

```
cd nextcloud/
```

```
sudo chown apache html
```

```
ls -l
total 0
drwxr-xr-x. 2 apache root 6 Dec  8 08:54 html
```









🌞 **Configurer PHP**

```
cd /etc/opt/remi/php74/
```

```
sudo nano php.ini
```



# III. NextCloud



🌞 **Récupérer Nextcloud**

```
cd
```

```
curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0   406k      0  0:06:12  0:06:12 --:--:--  996k
```



🌞 **Ranger la chambre**

```
sudo unzip nextcloud-21.0.1.zip -d /var/www/nextcloud/html/

```
```
cd /var/www/nextcloud/html/
```
```
sudo chown -R apache nextcloud/
```

```
ls -l
total 116
drwxr-xr-x. 43 apache root  4096 Apr  8  2021 3rdparty
drwxr-xr-x. 47 apache root  4096 Apr  8  2021 apps
-rw-r--r--.  1 apache root 17900 Apr  8  2021 AUTHORS
drwxr-xr-x.  2 apache root    67 Apr  8  2021 config
-rw-r--r--.  1 apache root  3900 Apr  8  2021 console.php
-rw-r--r--.  1 apache root 34520 Apr  8  2021 COPYING
drwxr-xr-x. 22 apache root  4096 Apr  8  2021 core
-rw-r--r--.  1 apache root  5122 Apr  8  2021 cron.php
-rw-r--r--.  1 apache root   156 Apr  8  2021 index.html
-rw-r--r--.  1 apache root  2960 Apr  8  2021 index.php
drwxr-xr-x.  6 apache root   125 Apr  8  2021 lib
-rw-r--r--.  1 apache root   283 Apr  8  2021 occ
drwxr-xr-x.  2 apache root    23 Apr  8  2021 ocm-provider
drwxr-xr-x.  2 apache root    55 Apr  8  2021 ocs
drwxr-xr-x.  2 apache root    23 Apr  8  2021 ocs-provider
-rw-r--r--.  1 apache root  3144 Apr  8  2021 public.php
-rw-r--r--.  1 apache root  5341 Apr  8  2021 remote.php
drwxr-xr-x.  4 apache root   133 Apr  8  2021 resources
-rw-r--r--.  1 apache root    26 Apr  8  2021 robots.txt
-rw-r--r--.  1 apache root  2446 Apr  8  2021 status.php
drwxr-xr-x.  3 apache root    35 Apr  8  2021 themes
drwxr-xr-x.  2 apache root    43 Apr  8  2021 updater
-rw-r--r--.  1 apache root   382 Apr  8  2021 version.php
```

```
cd ..
```
```
sudo mv /var/www/nextcloud/html/nextcloud/* /var/www/nextcloud/html/
```
```
sudo mv .htaccess /var/www/nextcloud/html/
```

```
sudo mv .user.ini /var/www/nextcloud/html/
```
```
ls -al
total 128
drwxr-xr-x. 15 apache root    4096 Dec  8 10:08 .
drwxr-xr-x.  3 apache root      18 Dec  8 08:54 ..
drwxr-xr-x. 43 apache root    4096 Apr  8  2021 3rdparty
drwxr-xr-x. 47 apache root    4096 Apr  8  2021 apps
-rw-r--r--.  1 apache root   17900 Apr  8  2021 AUTHORS
drwxr-xr-x.  2 apache root      85 Dec  8 09:43 config
-rw-r--r--.  1 apache root    3900 Apr  8  2021 console.php
-rw-r--r--.  1 apache root   34520 Apr  8  2021 COPYING
drwxr-xr-x. 22 apache root    4096 Apr  8  2021 core
-rw-r--r--.  1 apache root    5122 Apr  8  2021 cron.php
drwxr-xr-x.  2 apache apache    62 Dec  8 10:11 data
-rw-r--r--.  1 apache root    2734 Apr  8  2021 .htaccess
-rw-r--r--.  1 apache root     156 Apr  8  2021 index.html
-rw-r--r--.  1 apache root    2960 Apr  8  2021 index.php
drwxr-xr-x.  6 apache root     125 Apr  8  2021 lib
drwxr-xr-x.  2 apache root       6 Dec  8 10:08 nextcloud
-rw-r--r--.  1 apache root     283 Apr  8  2021 occ
drwxr-xr-x.  2 apache root      23 Apr  8  2021 ocm-provider
drwxr-xr-x.  2 apache root      55 Apr  8  2021 ocs
drwxr-xr-x.  2 apache root      23 Apr  8  2021 ocs-provider
-rw-r--r--.  1 apache root    3144 Apr  8  2021 public.php
-rw-r--r--.  1 apache root    5341 Apr  8  2021 remote.php
drwxr-xr-x.  4 apache root     133 Apr  8  2021 resources
-rw-r--r--.  1 apache root      26 Apr  8  2021 robots.txt
-rw-r--r--.  1 apache root    2446 Apr  8  2021 status.php
drwxr-xr-x.  3 apache root      35 Apr  8  2021 themes
drwxr-xr-x.  2 apache root      43 Apr  8  2021 updater
-rw-r--r--.  1 apache root     101 Apr  8  2021 .user.ini
-rw-r--r--.  1 apache root     382 Apr  8  2021 version.php
```

```
cd
```

```
sudo rm nextcloud-21.0.1.zip
```

## 4. Test


🌞 **Modifiez le fichier `hosts` de votre PC**

10.2.1.11     web.tp2.cesi               #nextcloud


🌞 **Tester l'accès à NextCloud et finaliser son install'**

```
curl web.tp2.cesi
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
        <head
 data-requesttoken="9inwr1dBDgZqEqjEyPfHUoH1jGexovQRjkbNq5otFiA=:kH6C6CcnVkQnJJmUscbwFsO8+ADVz8xm5AeBz8JDZm0=">
                <meta charset="utf-8">
                <title>
                Nextcloud               </title>
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
                                <meta name="apple-itunes-app" content="app-id=1125420102">
                                <meta name="theme-color" content="#0082c9">
                <link rel="icon" href="/core/img/favicon.ico">
                <link rel="apple-touch-icon" href="/core/img/favicon-touch.png">
                <link rel="mask-icon" sizes="any" href="/core/img/favicon-mask.svg" color="#0082c9">
                <link rel="manifest" href="/core/img/manifest.json">
                <link rel="stylesheet" href="/core/css/guest.css?v=ba222ded25d957b900c03bef914333cd">
                <script nonce="OWlud3IxZEJEZ1pxRXFqRXlQZkhVb0gxakdleG92UVJqa2JOcTVvdEZpQT06a0g2QzZDY25Wa1FuSkptVXNjYndGc084K0FEVno4eG01QWVCejhKRFptMD0=" defer src="/core/js/dist/main.js?v=ba222ded25d957b900c03bef914333cd"></script>
<script nonce="OWlud3IxZEJEZ1pxRXFqRXlQZkhVb0gxakdleG92UVJqa2JOcTVvdEZpQT06a0g2QzZDY25Wa1FuSkptVXNjYndGc084K0FEVno4eG01QWVCejhKRFptMD0=" defer src="/core/js/dist/install.js?v=ba222ded25d957b900c03bef914333cd"></script>
                        </head>
        <body id="body-login">
                <noscript>
        <div id="nojavascript">
                <div>
```

# Partie 2 : Sécurisation

# I. Serveur SSH


## 1. Conf SSH

```
su root
```

```
cd
```

```
mkdir .ssh
```

```
chmod 700 .ssh
```

```
PS C:\Users\ADMIN> cat ~/.ssh/id_rsa.pub | ssh root@10.2.1.11 -p 1230 "cat >> ~/.ssh/authorized_keys"
```

```
PS C:\Users\ADMIN> cat ~/.ssh/id_rsa.pub | ssh vincent@10.2.1.11 -p 1230 "cat >> ~/.ssh/authorized_keys"
```
```
PS C:\Users\ADMIN> ssh vincent@10.2.1.11 -p 1230
Activate the web console with: systemctl enable --now cockpit.socket
```
```
Last login: Wed Dec  8 11:23:28 2021 from 10.2.1.1
[vincent@web ~]$
```




🌞 **Modifier la conf du serveur SSH**

```
sudo nano /etc/ssh/sshd_config
```

#LoginGraceTime 2m
PermitRootLogin no
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no
PasswordAuthentication no

# algorithms
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-25$Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
hostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp$


## 2. Bonus : Fail2Ban



🌞 **Installez et configurez fail2ban**

```
sudo systemctl start firewalld
```

```
sudo dnf install fail2ban fail2ban-firewalld -y
sudo dnf install fail2ban fail2ban-firewalld -y
Last metadata expiration check: 1:21:40 ago on Wed 08 Dec 2021 12:09:49 PM CET.
Dependencies resolved.
=====================================================================================================================
 Package                           Architecture          Version                      Repository                Size
=====================================================================================================================
Installing:
 fail2ban                          noarch                0.11.2-1.el8                 epel                      19 k
 fail2ban-firewalld                noarch                0.11.2-1.el8                 epel                      19 k
```

```
sudo systemctl start fail2ban
```

```
systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 13:33:01 CET; 45s ago
     Docs: man:fail2ban(1)
  Process: 2588 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
 Main PID: 2590 (fail2ban-server)
    Tasks: 3 (limit: 11234)
   Memory: 12.7M
   CGroup: /system.slice/fail2ban.service
           └─2590 /usr/bin/python3.6 -s /usr/bin/fail2ban-server -xf start
```

```
sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service
```

```
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```

```
sudo nano /etc/fail2ban/jail.local

# "bantime" is the number of seconds that a host is banned.
bantime  = 1h

# A host is banned if it has generated "maxretry" during the last "findtime"
# seconds.
findtime  = 1h

# "maxretry" is the number of failures before a host get banned.
maxretry = 5

```

```
sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local

```

```
sudo systemctl restart fail2ban
```

```
systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 13:37:42 CET; 42s ago
     Docs: man:fail2ban(1)
  Process: 2650 ExecStop=/usr/bin/fail2ban-client stop (code=exited, status=0/SUCCESS)
  Process: 2652 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
 Main PID: 2654 (fail2ban-server)
    Tasks: 3 (limit: 11234)
   Memory: 11.0M
   CGroup: /system.slice/fail2ban.service
           └─2654 /usr/bin/python3.6 -s /usr/bin/fail2ban-server -xf start

```
```
sudo nano /etc/fail2ban/jail.d/sshd.local

[sshd]
enabled = true
bantime = 1d
maxretry = 3
```

```
sudo systemctl restart fail2ban
```

```
sudo fail2ban-client unban 10.2.1.12
1
```




# II. Serveur Web


## 1. Reverse Proxy



🌞 **Installer NGINX**

Nginx déjà installé car clonage 
```
sudo systemctl enable nginx
```
```
sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 14:56:03 CET; 56s ago
  Process: 1926 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 1923 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 1921 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 1927 (nginx)
    Tasks: 3 (limit: 11221)
   Memory: 5.1M
   CGroup: /system.slice/nginx.service
           ├─1927 nginx: master process /usr/sbin/nginx
           ├─1928 nginx: worker process
           └─1929 nginx: worker process

```
```
curl 10.2.1.13
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
      /*<![CDATA[*/
      body {
        background-color: #fff;
        color: #000;
        font-size: 0.9em;
        font-family: sans-serif, helvetica;
        margin: 0;
        padding: 0;
      }
      :link {
        color: #c00;
```

🌞 **Configurer NGINX comme reverse proxy**

